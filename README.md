# SpaceNet 6: MSAW数据集发布

## 简介
SpaceNet 6: MSAW数据集是一个结合了合成孔径雷达（SAR）和光电图像数据集的开放许可数据集。该数据集旨在利用计算机视觉和人工智能（AI）算法自动提取建筑足迹。数据集包含了来自Capella Space的0.5米分辨率SAR图像和Maxar WorldView-2卫星的0.5米分辨率光电图像。

## 数据集特点
- **SAR图像**：由Capella Space公司提供，包含48,000多个高质量的建筑足迹注释。
- **光电图像**：由Maxar WorldView-2卫星提供，覆盖了荷兰鹿特丹地区，总面积为236平方公里。
- **数据融合**：训练数据集同时包含SAR和EO图像，测试和评分数据集只包含SAR数据。

## 数据集结构
数据集模拟了真实世界的场景，其中历史的EO数据可能是可用的，但由于传感器的轨道不一致或云层覆盖，与SAR同时收集EO数据通常是不可能的。

## 数据文件
数据文件包括全色、多光谱、全色与多光谱、全色与红外、SAR数据等多种类型。

## 使用说明
用户可以通过官方网站下载数据集，但需要注册亚马逊账号。建议使用百度云下载，百度云链接为：https://pan.baidu.com/s/1-OzDW7DtjS6pBlSZkpIZJA (提取码：xkf0)。

## 应用场景
该数据集适用于研究SAR和光电图像数据的融合，特别是在超高分辨率下提取建筑足迹的应用。

## 参考文献
- 3D Basisregistratie Adressen en Gebouwen (3DBAG)数据集

## 版权声明
本文为博主原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接和本声明。